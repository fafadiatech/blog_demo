# -*- coding: utf-8 -*-
import os
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from hashlib import md5

from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client.scrapy_data
collection = db.voylla
collection.ensure_index("url")

BASE_DIR = "voylla_pages"

#Make sure we have directory created 
if not os.path.exists(BASE_DIR):
    os.mkdir(BASE_DIR)
log_file = open(os.path.join(BASE_DIR, "voylla.log"), "w")

class ThegudlookSpider(CrawlSpider):
    name = "voylla"
    allowed_domains = ["voylla.com"]
    start_urls = (
        'http://www.voylla.com/',
    )

    rules = (
        Rule(LinkExtractor(allow=(r'.*',r'.*\/products\/.*'),deny=(r'.*\/blog.voylla.com\/.*',r'.*\/deal\/.*',r'.*\/t\/.*',r'.*\/designers\/.*')), callback="parse_custom" ,follow=True),
    )

    def is_already_crawled(self, url):
        return collection.find({"url": url}).count() != 0

    def parse_custom(self, response):
        normalized_url = response.url
        if response.url.find("?") != -1:
            normalized_url = response.url.split("?")[0]

        if not self.is_already_crawled(normalized_url):
            file_hex = md5(normalized_url).hexdigest()
            filename = os.path.join(BASE_DIR, md5(normalized_url).hexdigest())
            with open(filename, 'wb') as f:
                f.write(response.body)

            rec = "%s\t%s\n" % (normalized_url, filename)
            log_file.write(rec)

            #Update MongoDB
            collection.insert({"url": normalized_url, "Hex_file": file_hex})
