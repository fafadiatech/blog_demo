# -*- coding: utf-8 -*-
import os
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from hashlib import md5

from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client.scrapy_data
collection = db.fabindia
collection.ensure_index("url")

BASE_DIR = "fabindia_pages"

#Make sure we have directory created 
if not os.path.exists(BASE_DIR):
    os.mkdir(BASE_DIR)
log_file = open(os.path.join(BASE_DIR, "fabindia.log"), "w")

class FabIndiaSpider(CrawlSpider):
    name = "fabindia"
    allowed_domains = ["fabindia.com"]
    start_urls = (
            'http://www.fabindia.com/',
        # 'http://www.fabindia.com/men/clothing/pyjamas-churidars.html',
        # 'http://www.fabindia.com/men/collections/festive.html',
        # 'http://www.fabindia.com/men/collections/khadi.html',
        # 'http://www.fabindia.com/men/collections/indigo.html',
        # 'http://www.fabindia.com/clothes-for-kids/girls-clothes/kurtas.html',
        # 'http://www.fabindia.com/clothes-for-kids/girls-clothes/churidar-sets.html',
        # 'http://www.fabindia.com/clothes-for-kids/girls-clothes/lehenga-sets.html',
        # 'http://www.fabindia.com/clothes-for-kids/girls-clothes/salwar-patiala.html',
        # 'http://www.fabindia.com/clothes-for-kids/girls-clothes/dresses.html',
        # 'http://www.fabindia.com/clothes-for-kids/girls-clothes/skirts-pants.html',
        # 'http://www.fabindia.com/clothes-for-kids/boys-clothes/kurtas.html',
        # 'http://www.fabindia.com/clothes-for-kids/boys-clothes/shirts.html',
        # 'http://www.fabindia.com/clothes-for-kids/boys-clothes/jackets.html',
        # 'http://www.fabindia.com/clothes-for-kids/boys-clothes/pyjamas-churidars.html',
        # 'http://www.fabindia.com/clothes-for-kids/infants.html',
        # 'http://www.fabindia.com/clothes-for-kids/jewellery.html',
        # 'http://www.fabindia.com/clothes-for-kids/collections/indigo-collection.html',
        # 'http://www.fabindia.com/clothes-for-kids/collections/festive.html',
        # 'http://www.fabindia.com/home-gifts/bed-linen/bedspreads.html',
        # 'http://www.fabindia.com/home-gifts/bed-linen/cushion-covers.html',
        # 'http://www.fabindia.com/home-gifts/bed-linen/quilts.html',
        # 'http://www.fabindia.com/home-gifts/table-linen/table-covers.html',
        # 'http://www.fabindia.com/home-gifts/table-linen/mats-napkins.html',
        # 'http://www.fabindia.com/home-gifts/table-linen/runners.html',
        # 'http://www.fabindia.com/home-gifts/curtains.html',
        # 'http://www.fabindia.com/home-gifts/bath-linen/bath-mats.html',
        # 'http://www.fabindia.com/home-gifts/bath-linen/towels.html',
        # 'http://www.fabindia.com/home-gifts/floor-coverings/dhurries.html',
        # 'http://www.fabindia.com/home-gifts/giftware/tableware.html',
        # 'http://www.fabindia.com/home-gifts/giftware/decoratives.html',
        # 'http://www.fabindia.com/home-gifts/giftware/lighting.html',
        # 'http://www.fabindia.com/home-gifts/giftware/stationery.html',
        # 'http://www.fabindia.com/home-gifts/giftware/others.html',
        # 'http://www.fabindia.com/home-gifts/collections/gara.html',
        # 'http://www.fabindia.com/home-gifts/collections/aangan.html',
        # 'http://www.fabindia.com/home-gifts/collections/tusrika.html',
        # 'http://www.fabindia.com/home-gifts/collections/khadi.html',
        # 'http://www.fabindia.com/furniture/bedroom/beds.html',
        # 'http://www.fabindia.com/furniture/bedroom/bedside-tables.html',
        # 'http://www.fabindia.com/furniture/bedroom/wardrobe.html',
        # 'http://www.fabindia.com/furniture/dining-room/cabinets.html',
        # 'http://www.fabindia.com/furniture/dining-room/dining-table.html',
        # 'http://www.fabindia.com/furniture/dining-room/dining-chair.html',
        # 'http://www.fabindia.com/furniture/living-room/benches-stools.html',
        # 'http://www.fabindia.com/furniture/living-room/bookshelves-cabinets.html',
        # 'http://www.fabindia.com/furniture/living-room/chairs.html',
        # 'http://www.fabindia.com/furniture/living-room/coffee-tables.html',
        # 'http://www.fabindia.com/furniture/living-room/side-tables.html',
        # 'http://www.fabindia.com/furniture/living-room/day-bed.html',
        # 'http://www.fabindia.com/furniture/living-room/entertainment-units.html',
        # 'http://www.fabindia.com/furniture/living-room/shoe-racks.html',
        # 'http://www.fabindia.com/furniture/living-room/sofa-sectionals.html',
        # 'http://www.fabindia.com/furniture/study-room/chairs.html',
        # 'http://www.fabindia.com/furniture/study-room/cabinets.html',
        # 'http://www.fabindia.com/furniture/study-room/console.html',
        # 'http://www.fabindia.com/furniture/study-room/writing-table.html',
        # 'http://www.fabindia.com/furniture/accessories.html',
        # 'http://www.fabindia.com/furniture/collections/simple-collection.html',
        # 'http://www.fabindia.com/personal-care/skin-care/facewashes.html',
        # 'http://www.fabindia.com/personal-care/skin-care/soaps.html',
        # 'http://www.fabindia.com/personal-care/skin-care/bodywashes.html',
        # 'http://www.fabindia.com/personal-care/skin-care/scrubs.html',
        # 'http://www.fabindia.com/personal-care/skin-care/face-packs.html',
        # 'http://www.fabindia.com/personal-care/skin-care/creams.html',
        # 'http://www.fabindia.com/personal-care/skin-care/lotions.html',
        # 'http://www.fabindia.com/personal-care/skin-care/facial-sprays.html',
        # 'http://www.fabindia.com/personal-care/skin-care/skin-toners.html',
        # 'http://www.fabindia.com/personal-care/skin-care/specials.html',
        # 'http://www.fabindia.com/personal-care/hair-care/shampoos.html',
        # 'http://www.fabindia.com/personal-care/hair-care/conditioners.html',
        # 'http://www.fabindia.com/personal-care/organic-personal-care.html   ',
        # 'http://www.fabindia.com/personal-care/gifts-travel-kits.html',
        # 'http://www.fabindia.com/personal-care/silk-protein-range.html',
        # 'http://www.fabindia.com/organics/teas-coffees.html',
        # 'http://www.fabindia.com/organics/herbs-seasonings.html',
        # 'http://www.fabindia.com/organics/grocery/flour.html',
        # 'http://www.fabindia.com/organics/grocery/spices-seeds.html',
        # 'http://www.fabindia.com/organics/grocery/grains.html',
        # 'http://www.fabindia.com/organics/pastas.html',
        # 'http://www.fabindia.com/organics/drinks/soups.html',
        # 'http://www.fabindia.com/organics/drinks/powdered-drinks.html',
        # 'http://www.fabindia.com/organics/pickles-chutney.html',
        # 'http://www.fabindia.com/organics/preserves.html',
        # 'http://www.fabindia.com/organics/sauces.html',
        # 'http://www.fabindia.com/organics/ready-to-eat/muesli.html',
        # 'http://www.fabindia.com/organics/ready-to-eat/snacks.html  ',
        # 'http://www.fabindia.com/organics/sweeteners/sugar.html',
        # 'http://www.fabindia.com/organics/sweeteners/honey.html',
        # 'http://www.fabindia.com/organics/ayurvedic-supplements.html',
        # 'http://www.fabindia.com/fabels/women/tops.html',
        # 'http://www.fabindia.com/fabels/women/shirts.html',
        # 'http://www.fabindia.com/fabels/women/dresses.html',
        # 'http://www.fabindia.com/fabels/women/palazzos.html',
        # 'http://www.fabindia.com/fabels/women/pants.html',
        # 'http://www.fabindia.com/fabels/women/skirts.html',
        # 'http://www.fabindia.com/fabels/women/jackets.html',
        # 'http://www.fabindia.com/fabels/women/tunics.html',
        # 'http://www.fabindia.com/fabels/women/stoles.html',
        # 'http://www.fabindia.com/fabels/accessories/jewellery.html',
        # 'http://www.fabindia.com/fabels/accessories/bags.html',
        # 'http://www.fabindia.com/new-arrivals/women.html',
        # 'http://www.fabindia.com/new-arrivals/men.html',
        # 'http://www.fabindia.com/new-arrivals/kids.html',
        # 'http://www.fabindia.com/new-arrivals/home-gifts.html',
        # 'http://www.fabindia.com/women/clothing/kurtas-kurtis.html',
        # 'http://www.fabindia.com/women/clothing/tunics.html',
        # 'http://www.fabindia.com/women/clothing/tops-shirts.html',
        # 'http://www.fabindia.com/women/clothing/jackets-coats.html',
        # 'http://www.fabindia.com/women/clothing/dresses.html',
        # 'http://www.fabindia.com/women/clothing/dupattas-stoles.html',
        # 'http://www.fabindia.com/women/clothing/shawls-mufflers.html',
        # 'http://www.fabindia.com/women/clothing/saris.html',
        # 'http://www.fabindia.com/women/clothing/salwars-churidars.html',
        # 'http://www.fabindia.com/women/clothing/pants-capris-palazzos.html',
        # 'http://www.fabindia.com/women/clothing/skirts.html',
        # 'http://www.fabindia.com/women/accessories/jewellery.html',
        # 'http://www.fabindia.com/women/accessories/bags.html',
        # 'http://www.fabindia.com/women/accessories/footwear.html',
        # 'http://www.fabindia.com/women/personal-care.html',
        # 'http://www.fabindia.com/women/fabels-womens.html',
        # 'http://www.fabindia.com/women/collections.html',
        # 'http://www.fabindia.com/women/collections/festive.html',
        # 'http://www.fabindia.com/women/collections/indigo-collection.html',
        # 'http://www.fabindia.com/men/clothing/kurtas.html',
        # 'http://www.fabindia.com/men/clothing/t-shirts.html',
        # 'http://www.fabindia.com/men/clothing/shirts.html',
        # 'http://www.fabindia.com/men/clothing/jackets.html',
        # 'http://www.fabindia.com/men/clothing/pants.html',
    )

    rules = (
        Rule(LinkExtractor(allow=(r'.*',r'.*\/.*-.*.html'), deny=(r'.*\/shopby\/.*',r'\/.+\/customer\/account\/login')), callback="parse_custom" ,follow=True),
    )

    def is_already_crawled(self, url):
        return collection.find({"url": url}).count() != 0

    def parse_custom(self, response):
        normalized_url = response.url
        if response.url.find("?") != -1:
            normalized_url = response.url.split("?")[0]

        if not self.is_already_crawled(normalized_url):
            file_hex = md5(normalized_url).hexdigest()
            filename = os.path.join(BASE_DIR, md5(normalized_url).hexdigest())
            with open(filename, 'wb') as f:
                f.write(response.body)

            rec = "%s\t%s\n" % (normalized_url, filename)
            log_file.write(rec)

            #Update MongoDB
            collection.insert({"url": normalized_url, "Hex_file": file_hex})
