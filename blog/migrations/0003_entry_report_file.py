# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-02-11 09:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20140704_0103'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='report_file',
            field=models.FileField(blank=True, null=True, upload_to=b'download_files'),
        ),
    ]
